var tesseractPlugin = {
    doOcr: function(successCallback, errorCallback, resultType, language) {

        return cordova.exec(successCallback, errorCallback, 'Tesseract', 'doOcr', [resultType.replace(/\\/g,'').replace('file:///', '/'), language || 'eng']);

     }
};

module.exports = tesseractPlugin;