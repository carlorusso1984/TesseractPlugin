package com.carlorusso.tesseract;
 
import java.io.*;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;
import android.app.Activity;
import android.content.Intent;
import android.content.Context;
import android.net.Uri;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.content.res.AssetManager;


import com.googlecode.tesseract.android.*;

public class TesseractPlugin extends CordovaPlugin {
    public static final String ACTION_DO_OCR = "doOcr";
    public static String pathToData;

    private void copyInputStreamToFile(InputStream in, File file) {
        try {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void copyAssetFolder(String fromAssetPath) {
        if (fromAssetPath == null) {
            fromAssetPath = "eng";
        }

        Context context = cordova.getActivity().getApplicationContext();
        AssetManager assetManager = context.getAssets();

        try {
            File tessdataFolder = new File(context.getFilesDir(), "tessdata");
            tessdataFolder.mkdir();
            File dataFile = new File(tessdataFolder, fromAssetPath + ".traineddata");

            InputStream instream = assetManager.open("tessdata/" + fromAssetPath + ".traineddata");
            copyInputStreamToFile(instream, dataFile);

            TesseractPlugin.pathToData = dataFile.toString().replaceAll("tessdata/" + fromAssetPath + ".traineddata", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {

        TesseractPlugin pluto = new TesseractPlugin();

        if (action.equals(ACTION_DO_OCR)) {
            return this.doOcr(args.getString(0), args.getString(1), callbackContext);
        }

        return false;
    }

    private boolean doOcr(final String imageUrl, final String language, final CallbackContext callbackContext) {
        System.out.println("Executing OCR on " + imageUrl);

        cordova.getThreadPool().execute(new Runnable() {
            public void run() {
                if (TesseractPlugin.pathToData == null || !(new File(TesseractPlugin.pathToData).exists())) {
                    copyAssetFolder(language);
                    System.out.println("new folder is " + TesseractPlugin.pathToData);
                }

                try {

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 4;
                    Bitmap bitmap = BitmapFactory.decodeFile(imageUrl, options);

                    TessBaseAPI baseApi = new TessBaseAPI();

                    baseApi.setDebug(true);

                    // DATA_PATH = Path to the storage
                    // lang = for which the language data exists, usually "eng"
                    baseApi.init(TesseractPlugin.pathToData, language);
                    // Eg. baseApi.init("/mnt/sdcard/tesseract/tessdata/eng.traineddata", "eng");
                    baseApi.setImage(bitmap);
                    String recognizedText = baseApi.getUTF8Text();
                    baseApi.end();

                    callbackContext.success(recognizedText);
                } catch (Exception e) {
                    System.err.println("Exception: " + e.getMessage());
                    callbackContext.error(e.getMessage());
                }
            }
        });

        return true;
    }
}