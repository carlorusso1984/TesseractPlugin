Usage:

// Take picture using device camera and retrieve image as file uri string
navigator.camera.getPicture(function(imageURI) {

    tesseract.doOcr(
        function nativePluginResultHandler(result) {
            alert("ok: " + result);

            var resultado = document.getElementById("resultado");

            resultado.innerHTML = result;
        },
        function nativePluginErrorHandler(error) {
            alert("error: " + error);
        },
        imageURI,
        'eng'
    );
}, function(message) {
    alert('Failed because: ' + message);
}, {
    quality: 100,
    destinationType: destinationType.FILE_URI
});
